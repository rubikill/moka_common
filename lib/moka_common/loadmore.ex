defmodule MokaCommon.Loadmore do
  import Ecto.Query
  import Ecto.Changeset

  @default_values %{limit: 25, max_id: nil}
  def default_values, do: @default_values

  @default_types %{
    limit: :integer,
    max_id: :integer
  }
  def default_types, do: @default_types

  defstruct Map.to_list(@default_values)

  def __changeset__, do: @default_types

  def validate(changeset) do
    changeset
    |> validate_number(:limit, less_than_or_equal_to: 100)
    |> validate_number(:limit, greater_than_or_equal_to: 1)
  end

  def changeset(model, params \\ %{}) do
    model
    |> cast(params, Map.keys(@default_values))
    |> validate()
  end

  def cast(params \\ %{}) do
    changeset(%__MODULE__{}, params)
    |> validate()
  end

  @doc """
  Load more query with give loadmore_params object
  """
  def new(query, repo, params) do
    changesetz = changeset(%__MODULE__{}, params)

    if changesetz.valid?() do
      data = apply_changes(changesetz)

      query =
        if is_nil(data.max_id) do
          query
        else
          where(query, [i], i.id < ^data.max_id)
        end

      entries =
        query
        |> limit(^data.limit)
        |> order_by([i], desc: :id)
        |> repo.all()

      l = length(entries)

      next_params =
        if l < data.limit do
          nil
        else
          last_entry = Enum.at(entries, l - 1)

          %{
            limit: data.limit,
            max_id: last_entry.id
          }
        end

      {:ok, %{entries: entries, next_params: next_params}}
    else
      {:error, :validation_failed, changesetz}
    end
  end
end
