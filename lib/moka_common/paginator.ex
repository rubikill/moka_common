defmodule MokaCommon.Paginator do
  import Ecto.Query
  alias MokaCommon.Repo
  defstruct entries: [], page: 1, size: 0, total: 0

  def new(query, repo, params) do
    page_number = params |> Map.get("page", 1) |> to_int
    page_size = params |> Map.get("size", 10) |> to_int

    %__MODULE__{
      entries: entries(query, repo, page_number, page_size),
      page: page_number,
      size: page_size,
      total: total_pages(query, repo, page_size)
    }
  end

  defp entries(query, repo, page_number, page_size) do
    offset = page_size * (page_number - 1)

    query
    |> limit(^page_size)
    |> offset(^offset)
    |> repo.all()
  end

  defp to_int(i) when is_integer(i), do: i

  defp to_int(s) when is_binary(s) do
    case Integer.parse(s) do
      {i, _} -> i
      :error -> :error
    end
  end

  defp total_pages(query, repo, page_size) do
    count =
      query
      |> exclude(:group_by)
      |> exclude(:order_by)
      |> exclude(:preload)
      |> exclude(:select)
      |> select([e], count(e.id))
      |> limit(1)
      |> repo.one()

    Float.ceil(count / page_size) |> round()
  end
end
