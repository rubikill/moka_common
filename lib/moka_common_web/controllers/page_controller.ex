defmodule MokaCommonWeb.PageController do
  use MokaCommonWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
